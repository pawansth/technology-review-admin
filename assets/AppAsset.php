<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css',
        'css/demo.css',
        'css/pe-icon-7-stroke.css',
        'css/animate.min.css',
        'css/light-bootstrap-dashboard.css',
        'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
        'http://fonts.googleapis.com/css?family=Roboto:400,700,300',
        'css/mobile.css',  

    ];
    public $js = [
        'js/bootstrap.min.js',
        // 'js/jquery-3.3.1.min.js',
        /** noticification plugin **/
        'js/bootstrap-notify.js',
        /** Control Center for Light Bootstrap Dashboard: scripts for the example pages etc **/
        'js/light-bootstrap-dashboard.js',
        'js/bootstrap-select.js',
        'js/demo.js',  
       
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
