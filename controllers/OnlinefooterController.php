<?php

namespace app\controllers;
use Yii;
class OnlinefooterController extends \yii\web\Controller
{
    public function actionIndex()
    {
         $exist = \app\models\Footer::find()->where(['id'=>Yii::$app->user->identity->id])->exists();
        if($exist){
            $model = \app\models\Footer::findOne(['id'=>Yii::$app->user->identity->id]);
        }else{
            $model = new \app\models\Footer();    
        }
    	 if($model->load(Yii::$app->request->post())){
             $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
if ($model->file ) {
$imagetoload = 'online'.time();

if( $model->file->saveAs('image/footer-image/' . $imagetoload . '.' . $model->file->extension)) 
{
$model->footer_image = $imagetoload . '.' . $model->file->extension; 
}
}
            $model->save();
            return $this->redirect(['index']);
           }else{
        return $this->render('index',[
        	'model'=>$model,
        ]);

    }
    }

}
