<?php

namespace app\controllers;
use Yii;
class OnlinefrontadsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $exist = \app\models\FrontAds::find()->where(['id'=>Yii::$app->user->identity->id])->exists();
        if($exist){
            $model = \app\models\FrontAds::findOne(['id'=>Yii::$app->user->identity->id]);
        }else{
            $model = new \app\models\FrontAds();    
        }
    	 if($model->load(Yii::$app->request->post())){

$model->save();
return $this->redirect(['index']);
} else{
        return $this->render('index',[
        	'model'=>$model,
        ]);

    }
    }

}
