<?php

namespace app\controllers;
use Yii;
class OnlineheaderController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $exist = \app\models\Header::find()->where(['id'=>Yii::$app->user->identity->id])->exists();
        if($exist){
            $model = \app\models\Header::findOne(['id'=>Yii::$app->user->identity->id]);
        }else{
            $model = new \app\models\Header();    
        }
    	 if($model->load(Yii::$app->request->post())){
             $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
if ($model->file ) {
$imagetoload = 'header'.time();

if( $model->file->saveAs('image/header-image/' . $imagetoload . '.' . $model->file->extension)) 
{
$model->header = $imagetoload . '.' . $model->file->extension; 
}
}
            $model->save();
            return $this->redirect(['index']);
           }else{
        return $this->render('index',[
        	'model'=>$model,
        ]);

    }
    }

}
