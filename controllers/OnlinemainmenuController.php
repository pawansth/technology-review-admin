<?php

namespace app\controllers;

use Yii;
use \app\models\CategoriesMenu;

class OnlinemainmenuController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new \app\models\CategoriesMenuSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->query->andFilterWhere(['status'=>1]);

return $this->render('index', [
'searchModel' => $searchModel,
'dataProvider' => $dataProvider,

]);
    }

    public function actionAddmainmenu()
    {
    	 $model = new \app\models\CategoriesMenu();
if($model->load(Yii::$app->request->post())){

$model->save();
return $this->redirect(['index']);
} else {
return $this->render('addmainmenu', [
'model' => $model,
]);
}
    }

    public function actionMainmenuupdate($id)
    {
    	 $model=\app\models\CategoriesMenu::findOne(['id'=>$id]);
            if($model->load(Yii::$app->request->post())){
        $model->save();
    return $this->redirect(['index']);
    } else {
    return $this->render('addmainmenu', [
    'model' => $model,
    ]);
    }
    }

    public function actionMainmenudelete($id)
    {
    	$model=\app\models\CategoriesMenu::findOne(['id'=>$id]);
$model->status=0;
$model->save();
return $this->redirect(['index']);
    }

    public function actionViewmainmenu($id)
    {
    	$model= \app\models\CategoriesMenu::findOne(['id'=>$id]);
        return $this->render('viewmainmenu',['model'=>$model]);
    }

}
