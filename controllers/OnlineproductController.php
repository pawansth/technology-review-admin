<?php

namespace app\controllers;
use Yii;
use \app\models\CategoriesSubmenu;
use yii\web\Controller;
use app\models\Product;


class OnlineproductController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new \app\models\ProductSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->query->andFilterWhere(['status'=>1]);

return $this->render('index', [
'searchModel' => $searchModel,
'dataProvider' => $dataProvider,

]);
    }

   public function actionSubcat() {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    if (isset(Yii::$app->request->post()['depdrop_parents'])) {
        $parents = Yii::$app->request->post('depdrop_parents');
        if ($parents != null) {
            $cat_id = $parents[0];
            return [
                'output' => CategoriesSubmenu::getSubCatList($cat_id, true),
                'selected' => '',
            ];
        }
    }
    return ['output' => '', 'selected' => ''];
}

    

    public function actionAddproduct()
    {
    	 $model = new \app\models\Product();

    if ($model->load(Yii::$app->request->post())) {
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file'); 
            $model->file1 = \yii\web\UploadedFile::getInstance($model, 'file1'); 
            $model->file2 = \yii\web\UploadedFile::getInstance($model, 'file2'); 
            $model->file3 = \yii\web\UploadedFile::getInstance($model, 'file3'); 
         
        if ($model->file ) {
$imagetoload = 'productos'.time();

if( $model->file->saveAs('image/products/' . $imagetoload . '.' . $model->file->extension)) 
{
$model->product_image1 = $imagetoload . '.' . $model->file->extension; 
}
}
        if ($model->file1 ) {
$imagetoload = 'products'.time();

if( $model->file1->saveAs('image/products/' . $imagetoload . '.' . $model->file1->extension)) 
{
$model->product_image2 = $imagetoload . '.' . $model->file1->extension; 
}
}
if ($model->file2 ) {
$imagetoload = 'productss'.time();

if( $model->file2->saveAs('image/products/' . $imagetoload . '.' . $model->file2->extension)) 
{
$model->product_image3 = $imagetoload . '.' . $model->file2->extension; 
}
}
if ($model->file3 ) {
$imagetoload = 'productsss'.time();

if( $model->file3->saveAs('image/products/' . $imagetoload . '.' . $model->file3->extension)) 
{
$model->product_image4 = $imagetoload . '.' . $model->file3->extension; 
}
}
        $model->save();
        return $this->redirect(['index']);
    } else {
        return $this->render('addproduct', [
            'model' => $model,
            
        ]);
    }
    }


    public function actionProductupdate($id)
    {
    	$model=\app\models\Product::findOne(['id'=>$id]);
            if($model->load(Yii::$app->request->post())){
        $model->save();
    return $this->redirect(['index']);
    } else {
    return $this->render('addproduct', [
    'model' => $model,
    ]);
    }
    }

    public function actionProductdelete($id)
    {
    	$model=\app\models\Product::findOne(['id'=>$id]);
$model->status=0;
$model->save();
return $this->redirect(['index']);
    }

    public function actionViewproduct($id)
    {
        $model= \app\models\Product::findOne(['id'=>$id]);
        return $this->render('viewproduct',['model'=>$model]);
    }

    
}


