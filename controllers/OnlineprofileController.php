<?php

namespace app\controllers;
use Yii;
class OnlineprofileController extends \yii\web\Controller
{
    public function actionIndex()
    {
         $exist = \app\models\Userform::find()->where(['id'=>Yii::$app->user->identity->id])->exists();
        if($exist){
            $model = \app\models\Userform::findOne(['id'=>Yii::$app->user->identity->id]);
        }else{
            $model = new \app\models\Userform();    
        }
    	 if($model->load(Yii::$app->request->post())){
               $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
if ($model->file ) {
$imagetoload = 'profile'.time();

if( $model->file->saveAs('image/profile/' . $imagetoload . '.' . $model->file->extension)) 
{
$model->image = $imagetoload . '.' . $model->file->extension; 
}
}
            $model->save();
            return $this->redirect(['index']);
           }else{
        return $this->render('index',[
        	'model'=>$model,
        ]);

    }
    }

}
