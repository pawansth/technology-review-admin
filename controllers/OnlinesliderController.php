<?php

namespace app\controllers;
use Yii;
use yii\web\Controller;
use \app\models\Slider;

class OnlinesliderController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	 $searchModel = new \app\models\SliderSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->query->andFilterWhere(['status'=>1]);
        return $this->render('index',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        ]);
    }

    public function actionAddslider()
    {
    	$model = new \app\models\Slider();

    if ($model->load(Yii::$app->request->post())) {
           $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
           $model->file1 = \yii\web\UploadedFile::getInstance($model, 'file1');
           $model->file2 = \yii\web\UploadedFile::getInstance($model, 'file2'); 
           $model->file3 = \yii\web\UploadedFile::getInstance($model, 'file3'); 
         $model->files = \yii\web\UploadedFile::getInstance($model, 'files');
         if ($model->file ) {
$imagetoload = 'header'.time();

if( $model->file->saveAs('image/slider/' . $imagetoload . '.' . $model->file->extension)) 
{
$model->slider_image = $imagetoload . '.' . $model->file->extension; 
}
} 
        if ($model->files ) {
$imagetoload = 'slider'.time();

if( $model->files->saveAs('image/slider/' . $imagetoload . '.' . $model->files->extension)) 
{
$model->product_image1 = $imagetoload . '.' . $model->files->extension; 
}
}
 if ($model->file1 ) {
$imagetoload = 'sliders'.time();

if( $model->file1->saveAs('image/slider/' . $imagetoload . '.' . $model->file1->extension)) 
{
$model->product_image2 = $imagetoload . '.' . $model->file1->extension; 
}
}
 if ($model->file2 ) {
$imagetoload = 'sliderss'.time();

if( $model->file2->saveAs('image/slider/' . $imagetoload . '.' . $model->file2->extension)) 
{
$model->product_image3 = $imagetoload . '.' . $model->file2->extension; 
}
}
 if ($model->file3 ) {
$imagetoload = 'slidersss'.time();

if( $model->file3->saveAs('image/slider/' . $imagetoload . '.' . $model->file3->extension)) 
{
$model->product_image4 = $imagetoload . '.' . $model->file3->extension; 
}
}
        
        $model->save();
        return $this->redirect(['index']);
    } else {
        return $this->render('addslider', [
            'model' => $model,
            
        ]);
    }
    }
    public function actionSliderupdate($id)
    {
        $model=\app\models\Slider::findOne(['id'=>$id]);
            if($model->load(Yii::$app->request->post())){
        $model->save();
    return $this->redirect(['index']);
    } else {
    return $this->render('addslider', [
    'model' => $model,
    ]);
    }
    }
    public function actionSliderdelete($id)
    {
    	$model=\app\models\Slider::findOne(['id'=>$id]);
$model->status=0;
$model->save();
return $this->redirect(['index']);
    }
    public function actionViewslider($id)
    {
    	$model= \app\models\Slider::findOne(['id'=>$id]);
        return $this->render('viewslider',['model'=>$model]);
    }


}
