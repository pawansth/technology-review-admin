<?php

namespace app\controllers;
use Yii;
class OnlinesubmenuController extends \yii\web\Controller
{
    public function actionIndex()
    {
         $searchModel = new \app\models\CategoriesSubmenuSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->query->andFilterWhere(['status'=>1]);

return $this->render('index', [
'searchModel' => $searchModel,
'dataProvider' => $dataProvider,

]);
    }

   public function actionAddsubmenu()
    {
    	 $model = new \app\models\CategoriesSubmenu();
if($model->load(Yii::$app->request->post())){

$model->save();
return $this->redirect(['index']);
} else {
return $this->render('addsubmenu', [
'model' => $model,
]);
}
    }

    public function actionSubmenuupdate($id)
    {
    	 $model=\app\models\CategoriesSubmenu::findOne(['id'=>$id]);
            if($model->load(Yii::$app->request->post())){
        $model->save();
    return $this->redirect(['index']);
    } else {
    return $this->render('addsubmenu', [
    'model' => $model,
    ]);
    }
    }

    public function actionSubmenudelete($id)
    {
    	$model=\app\models\CategoriesSubmenu::findOne(['id'=>$id]);
$model->status=0;
$model->save();
return $this->redirect(['index']);
    }

    public function actionViewsubmenu($id)
    {
    	$model= \app\models\CategoriesSubmenu::findOne(['id'=>$id]);
        return $this->render('viewsubmenu',['model'=>$model]);
    }

}
