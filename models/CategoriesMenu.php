<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories_menu".
 *
 * @property int $id
 * @property string $name
 * @property int $priority
 * @property int $status
 *
 * @property CategoriesSubmenu[] $categoriesSubmenus
 * @property Product[] $products
 */
class CategoriesMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'priority', 'status'], 'required'],
            [['priority', 'status'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'priority' => 'Priority',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesSubmenus()
    {
        return $this->hasMany(CategoriesSubmenu::className(), ['categories_menu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['categories_menu' => 'id']);
    }

    public static function getCategories()
    {
    return Self::find()->where(['status'=>1])->select(['name', 'id'])->indexBy('id')->column();
    }
}
