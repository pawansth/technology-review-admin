<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories_submenu".
 *
 * @property int $id
 * @property string $name
 * @property int $priority
 * @property int $categories_menu
 * @property int $status
 *
 * @property CategoriesMenu $categoriesMenu
 * @property Product[] $products
 */
class CategoriesSubmenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories_submenu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'priority', 'categories_menu', 'status'], 'required'],
            [['priority', 'categories_menu', 'status'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['categories_menu'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriesMenu::className(), 'targetAttribute' => ['categories_menu' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'priority' => 'Priority',
            'categoriesMenu.name' => 'Categories Menu',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesMenu()
    {
        return $this->hasOne(CategoriesMenu::className(), ['id' => 'categories_menu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['sub_menu' => 'id']);
    }

    public static function getSubCatList($cat_id)
    {
    $subCategory = self::find()
        ->where(['categories_menu' => $cat_id, 'status'=>1])
        ->select(['id', 'name'])
        ->asArray()
        ->all();

    return $subCategory;
    }

    public static function getCategoriesSubmenus($cat_id)
    {
    return Self::find()->select(['name', 'id'])->where(['categories_menu' => $cat_id, 'status'=>1])->indexBy('id')->column();
    }
}
