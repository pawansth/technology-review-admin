<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "footer".
 *
 * @property int $id
 * @property string $footer_image
 * @property string $footer_descrip
 * @property string $social_link1
 * @property string $socail_link2
 * @property string $social_link3
 * @property string $social_link4
 * @property string $social_link5
 * @property string $social_link6
 * @property string $email
 * @property string $location
 * @property string $phone_number
 * @property int $status
 */
class Footer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'footer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['footer_descrip'], 'string'],
            [['status'], 'required'],
            [['status'], 'integer'],
            [['footer_image', 'social_link1', 'socail_link2', 'social_link3', 'social_link4', 'social_link5', 'social_link6', 'email', 'location', 'phone_number'], 'string', 'max' => 100],
            [['file'],'file'],
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'footer_image' => 'Footer Image',
            'footer_descrip' => 'Footer Descrip',
            'social_link1' => 'Social Link1',
            'socail_link2' => 'Socail Link2',
            'social_link3' => 'Social Link3',
            'social_link4' => 'Social Link4',
            'social_link5' => 'Social Link5',
            'social_link6' => 'Social Link6',
            'email' => 'Email',
            'location' => 'Location',
            'phone_number' => 'Phone Number',
            'status' => 'Status',
        ];
    }
}
