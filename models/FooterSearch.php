<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Footer;

/**
 * FooterSearch represents the model behind the search form of `app\models\Footer`.
 */
class FooterSearch extends Footer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['footer_image', 'footer_descrip', 'social_link1', 'socail_link2', 'social_link3', 'social_link4', 'social_link5', 'social_link6', 'email', 'location', 'phone_number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Footer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'footer_image', $this->footer_image])
            ->andFilterWhere(['like', 'footer_descrip', $this->footer_descrip])
            ->andFilterWhere(['like', 'social_link1', $this->social_link1])
            ->andFilterWhere(['like', 'socail_link2', $this->socail_link2])
            ->andFilterWhere(['like', 'social_link3', $this->social_link3])
            ->andFilterWhere(['like', 'social_link4', $this->social_link4])
            ->andFilterWhere(['like', 'social_link5', $this->social_link5])
            ->andFilterWhere(['like', 'social_link6', $this->social_link6])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number]);

        return $dataProvider;
    }
}
