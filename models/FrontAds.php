<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "front_ads".
 *
 * @property int $id
 * @property string $slider_ads
 * @property string $front_one
 * @property string $front_two
 * @property string $front_three
 * @property string $front_four
 * @property string $front_five
 * @property string $front_six
 * @property string $front_eight
 * @property int $status
 */
class FrontAds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'front_ads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'integer'],
            [['slider_ads', 'front_one', 'front_two', 'front_three', 'front_four', 'front_five', 'front_six', 'front_eight'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slider_ads' => 'First Slider ads',
            'front_one' => 'Second slider ads',
            'front_two' => 'Front 1',
            'front_three' => 'Front 2',
            'front_four' => 'Front 3',
            'front_five' => 'Front 4',
            'front_six' => 'Front 5',
            'front_eight' => 'Front 6',
            'status' => 'Status',
        ];
    }
}
