<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FrontAds;

/**
 * FrontAdsSearch represents the model behind the search form of `app\models\FrontAds`.
 */
class FrontAdsSearch extends FrontAds
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['slider_ads', 'front_one', 'front_two', 'front_three', 'front_four', 'front_five', 'front_six', 'front_eight'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FrontAds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'slider_ads', $this->slider_ads])
            ->andFilterWhere(['like', 'front_one', $this->front_one])
            ->andFilterWhere(['like', 'front_two', $this->front_two])
            ->andFilterWhere(['like', 'front_three', $this->front_three])
            ->andFilterWhere(['like', 'front_four', $this->front_four])
            ->andFilterWhere(['like', 'front_five', $this->front_five])
            ->andFilterWhere(['like', 'front_six', $this->front_six])
            ->andFilterWhere(['like', 'front_eight', $this->front_eight]);

        return $dataProvider;
    }
}
