<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "header".
 *
 * @property int $id
 * @property string $title
 * @property string $header
 * @property string $logo
 * @property string $extra
 * @property int $status
 */
class Header extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'header';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
            [['status'], 'required'],
            [['status'], 'integer'],
            [['header', 'logo', 'extra'], 'string', 'max' => 50],
            [['file'],'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'header' => 'Header',
            'logo' => 'Logo',
            'extra' => 'Extra',
            'status' => 'Status',
        ];
    }
}
