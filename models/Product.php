<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $product_name
 * @property string $product_price
 * @property string $product_brand
 * @property string $product_subdescrip
 * @property string $affiliate_link1
 * @property string $affiliate_link2
 * @property string $affiliate_link3
 * @property string $affiliate_link4
 * @property string $long_descrip
 * @property string $detail
 * @property int $categories_menu
 * @property int $status
 * @property string $product_image1
 * @property string $product_image2
 * @property string $product_image3
 * @property string $product_image4
 * @property int $sub_menu
 * @property string $affiliate_price
 * @property string $affiliate_price1
 *
 * @property CategoriesMenu $categoriesMenu
 * @property CategoriesSubmenu $subMenu
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public $file1;
    public $file2;
    public $file3;
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'product_subdescrip', 'categories_menu', 'status', 'sub_menu'], 'required'],
            [['product_subdescrip', 'long_descrip', 'detail'], 'string'],
            [['categories_menu', 'status', 'sub_menu'], 'integer'],
            [['product_name', 'affiliate_link1', 'affiliate_link2', 'affiliate_link3', 'affiliate_link4', 'affiliate_price', 'affiliate_price1'], 'string', 'max' => 100],
            [['product_price', 'product_brand', 'product_image1', 'product_image2', 'product_image3', 'product_image4'], 'string', 'max' => 50],
            [['categories_menu'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriesMenu::className(), 'targetAttribute' => ['categories_menu' => 'id']],
            [['sub_menu'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriesSubmenu::className(), 'targetAttribute' => ['sub_menu' => 'id']],
            [['file'],'file'],
            [['file1'],'file'],
            [['file2'],'file'],
            [['file3'],'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_name' => 'Product Name',
            'product_price' => 'Product Price',
            'product_brand' => 'Product Brand',
            'product_subdescrip' => 'Product Subdescrip',
            'affiliate_link1' => 'Affiliate Link1',
            'affiliate_link2' => 'Affiliate Link2',
            'affiliate_link3' => 'Affiliate Link3',
            'affiliate_link4' => 'Affiliate Link4',
            'long_descrip' => 'Long Descrip',
            'detail' => 'Detail',
            'categories_menu' => 'Categories Menu',
            'status' => 'Status',
            'product_image1' => 'Product Image1',
            'product_image2' => 'Product Image2',
            'product_image3' => 'Product Image3',
            'product_image4' => 'Product Image4',
            'sub_menu' => 'Sub Menu',
            'affiliate_price' => 'Affiliate Price',
            'affiliate_price1' => 'Affiliate Price1',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesMenu()
    {
        return $this->hasOne(CategoriesMenu::className(), ['id' => 'categories_menu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubMenu()
    {
        return $this->hasOne(CategoriesSubmenu::className(), ['id' => 'sub_menu']);
    }
}
