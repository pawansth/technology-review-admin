<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'categories_menu', 'status', 'sub_menu'], 'integer'],
            [['product_name', 'product_price', 'product_brand', 'product_subdescrip', 'affiliate_link1', 'affiliate_link2', 'affiliate_link3', 'affiliate_link4', 'long_descrip', 'detail', 'product_image1', 'product_image2', 'product_image3', 'product_image4', 'affiliate_price', 'affiliate_price1'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categories_menu' => $this->categories_menu,
            'status' => $this->status,
            'sub_menu' => $this->sub_menu,
        ]);

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_price', $this->product_price])
            ->andFilterWhere(['like', 'product_brand', $this->product_brand])
            ->andFilterWhere(['like', 'product_subdescrip', $this->product_subdescrip])
            ->andFilterWhere(['like', 'affiliate_link1', $this->affiliate_link1])
            ->andFilterWhere(['like', 'affiliate_link2', $this->affiliate_link2])
            ->andFilterWhere(['like', 'affiliate_link3', $this->affiliate_link3])
            ->andFilterWhere(['like', 'affiliate_link4', $this->affiliate_link4])
            ->andFilterWhere(['like', 'long_descrip', $this->long_descrip])
            ->andFilterWhere(['like', 'detail', $this->detail])
            ->andFilterWhere(['like', 'product_image1', $this->product_image1])
            ->andFilterWhere(['like', 'product_image2', $this->product_image2])
            ->andFilterWhere(['like', 'product_image3', $this->product_image3])
            ->andFilterWhere(['like', 'product_image4', $this->product_image4])
            ->andFilterWhere(['like', 'affiliate_price', $this->affiliate_price])
            ->andFilterWhere(['like', 'affiliate_price1', $this->affiliate_price1]);

        return $dataProvider;
    }
}
