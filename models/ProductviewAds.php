<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productview_ads".
 *
 * @property int $id
 * @property string $view_one
 * @property string $view_two
 * @property string $view_three
 * @property string $view_four
 * @property string $view_five
 * @property string $view_six
 * @property int $status
 */
class ProductviewAds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productview_ads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'integer'],
            [['view_one', 'view_two', 'view_three', 'view_four', 'view_five', 'view_six'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'view_one' => 'View One',
            'view_two' => 'View Two',
            'view_three' => 'View Three',
            'view_four' => 'View Four',
            'view_five' => 'View Five',
            'view_six' => 'View Six',
            'status' => 'Status',
        ];
    }
}
