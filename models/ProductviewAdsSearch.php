<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductviewAds;

/**
 * ProductviewAdsSearch represents the model behind the search form of `app\models\ProductviewAds`.
 */
class ProductviewAdsSearch extends ProductviewAds
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['view_one', 'view_two', 'view_three', 'view_four', 'view_five', 'view_six'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductviewAds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'view_one', $this->view_one])
            ->andFilterWhere(['like', 'view_two', $this->view_two])
            ->andFilterWhere(['like', 'view_three', $this->view_three])
            ->andFilterWhere(['like', 'view_four', $this->view_four])
            ->andFilterWhere(['like', 'view_five', $this->view_five])
            ->andFilterWhere(['like', 'view_six', $this->view_six]);

        return $dataProvider;
    }
}
