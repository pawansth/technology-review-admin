<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\base\Model;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $title
 * @property string $product_name
 * @property string $product_price
 * @property string $product_brand
 * @property string $product_subdescrip
 * @property string $affiliate_link1
 * @property string $affiliate_link2
 * @property string $affiliate_link3
 * @property string $affiliate_link4
 * @property string $long_descrip
 * @property string $detail
 * @property string $slider_image
 * @property string $product_image1
 * @property string $product_image2
 * @property string $product_image3
 * @property string $product_image4
 * @property int $status
 * @property string $affiliate_price1
 * @property string $affiliate_price2
 * @property string $extra_detail
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $files;
    public $file3;
    public $file2;
    public $file1;
    public $file;
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_subdescrip', 'long_descrip', 'detail', 'extra_detail'], 'string'],
            [['status'], 'required'],
            [['status'], 'integer'],
            [['title', 'product_name', 'product_price', 'product_brand', 'slider_image', 'product_image1', 'product_image2', 'product_image3', 'product_image4'], 'string', 'max' => 50],
            [['affiliate_link1', 'affiliate_link2', 'affiliate_link3', 'affiliate_link4', 'affiliate_price1', 'affiliate_price2'], 'string', 'max' => 100],
            [['file'], 'file'],
            [['file1'],'file'],
            [['file2'],'file'],
            [['file3'],'file'],
            [['files'],'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'product_name' => 'Product Name',
            'product_price' => 'Product Price',
            'product_brand' => 'Product Brand',
            'product_subdescrip' => 'Product Subdescrip',
            'affiliate_link1' => 'amazon link',
            'affiliate_link2' => 'Amazon Price',
            'affiliate_link3' => 'flipkart link',
            'affiliate_link4' => 'flipkart price',
            'affiliate_price1' => 'extra link',
            'affiliate_price2' => 'extra price',
            'long_descrip' => 'Long Descrip',
            'detail' => 'Detail',
            'slider_image' => 'Slider Image',
            'files' => 'Product Image1',
            'file1' => 'Product Image2',
            'file2' => 'Product Image3',
            'file3' => 'Product Image4',
            'status' => 'Status',
            'extra_detail' => 'Extra Detail',
            'file'=>'Slider Image',
            
        ];
    }
}
