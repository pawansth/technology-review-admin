<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userform".
 *
 * @property int $id
 * @property string $full_name
 * @property string $per_address
 * @property string $temp_address
 * @property string $dob
 * @property string $phone_number
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $image
 * @property string $phone_number2
 * @property string $detail
 * @property string $authKey
 * @property string $token
 * @property int $status
 */
class Userform extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'userform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'status'], 'required'],
            [['detail'], 'string'],
            [['status'], 'integer'],
            [['full_name', 'per_address', 'temp_address', 'username', 'password', 'email', 'image'], 'string', 'max' => 50],
            [['dob'], 'string', 'max' => 10],
            [['phone_number', 'phone_number2', 'authKey', 'token'], 'string', 'max' => 30],
            [['file'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'per_address' => 'Per Address',
            'temp_address' => 'Temp Address',
            'dob' => 'Dob',
            'phone_number' => 'Phone Number',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'image' => 'Image',
            'phone_number2' => 'Phone Number2',
            'detail' => 'Detail',
            'authKey' => 'Auth Key',
            'token' => 'Token',
            'status' => 'Status',
        ];
    }

     public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // foreach (self::$users as $user) {
        //     if ($user['accessToken'] === $token) {
        //         return new static($user);
        //     }
        // }

        return $this->token;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
    
       
        return self::find()->where(['username'=>$username])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return $this->password === $password;
    }
}
