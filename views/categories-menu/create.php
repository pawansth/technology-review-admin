<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CategoriesMenu */

$this->title = 'Create Categories Menu';
$this->params['breadcrumbs'][] = ['label' => 'Categories Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
