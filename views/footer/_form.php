<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Footer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="footer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'footer_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer_descrip')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'social_link1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'socail_link2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'social_link3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'social_link4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'social_link5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'social_link6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
