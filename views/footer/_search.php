<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FooterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="footer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'footer_image') ?>

    <?= $form->field($model, 'footer_descrip') ?>

    <?= $form->field($model, 'social_link1') ?>

    <?= $form->field($model, 'socail_link2') ?>

    <?php // echo $form->field($model, 'social_link3') ?>

    <?php // echo $form->field($model, 'social_link4') ?>

    <?php // echo $form->field($model, 'social_link5') ?>

    <?php // echo $form->field($model, 'social_link6') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'phone_number') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
