<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FooterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Footers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Footer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'footer_image',
            'footer_descrip:ntext',
            'social_link1',
            'socail_link2',
            //'social_link3',
            //'social_link4',
            //'social_link5',
            //'social_link6',
            //'email:email',
            //'location',
            //'phone_number',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
