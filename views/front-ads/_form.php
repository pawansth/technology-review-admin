<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FrontAds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="front-ads-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'slider_ads')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'front_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'front_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'front_three')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'front_four')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'front_five')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'front_six')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'front_eight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
