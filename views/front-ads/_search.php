<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FrontAdsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="front-ads-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'slider_ads') ?>

    <?= $form->field($model, 'front_one') ?>

    <?= $form->field($model, 'front_two') ?>

    <?= $form->field($model, 'front_three') ?>

    <?php // echo $form->field($model, 'front_four') ?>

    <?php // echo $form->field($model, 'front_five') ?>

    <?php // echo $form->field($model, 'front_six') ?>

    <?php // echo $form->field($model, 'front_eight') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
