<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FrontAds */

$this->title = 'Create Front Ads';
$this->params['breadcrumbs'][] = ['label' => 'Front Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="front-ads-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
