<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FrontAdsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Front Ads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="front-ads-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Front Ads', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'slider_ads',
            'front_one',
            'front_two',
            'front_three',
            //'front_four',
            //'front_five',
            //'front_six',
            //'front_eight',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
