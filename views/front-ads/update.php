<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FrontAds */

$this->title = 'Update Front Ads: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Front Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="front-ads-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
