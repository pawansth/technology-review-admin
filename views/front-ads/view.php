<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FrontAds */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Front Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="front-ads-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'slider_ads',
            'front_one',
            'front_two',
            'front_three',
            'front_four',
            'front_five',
            'front_six',
            'front_eight',
            'status',
        ],
    ]) ?>

</div>
