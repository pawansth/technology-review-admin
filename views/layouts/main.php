<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
     <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <meta name="description" content="">
    <meta name="author" content="">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fix-header fix-sidebar card-no-border">
<?php $this->beginBody() ?>
 <div class="wrapper">
 <?php  if(!Yii::$app->user->isGuest){ ?>
     <div class="sidebar" data-color="blue" data-image="images/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Mobile
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="<?= \yii\helpers\Url::to(['site/index'])?>">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="<?=Yii::$app->homeUrl ?>?r=/onlineheader/index">
                        <i class="pe-7s-news-paper"></i>
                        <p>Header</p>
                    </a>
                </li>
                <li>
                    <a href="<?=Yii::$app->homeUrl ?>?r=/site/forproduct">
                       <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <p>Products</p>
                    </a>
                </li>
                <li>
                    <a href="<?=Yii::$app->homeUrl ?>?r=/onlinefooter/index">
                        <i class="pe-7s-bell"></i>
                        <p>Footers</p>
                    </a>
                </li>
                <li>
                    <a href="<?=Yii::$app->homeUrl ?>?r=/site/ads">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                        <p>ads</p>
                    </a>
                </li>
                <li>
                    <a href="<?=Yii::$app->homeUrl ?>?r=/onlineslider/index">
                        <i class="fa fa-desktop" aria-hidden="true"></i>
                        <p>Slider</p>
                    </a>
                </li>
                
            </ul>
        </div>
    </div>

    
       
        
<div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= \yii\helpers\Url::to(['site/index'])?>">
                        <i class="fa fa-home" aria-hidden="true"></i>
                    Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    

                    <ul class="nav navbar-nav navbar-right">
                        <li style="text-align: center;">
                            <a href="<?=Yii::$app->homeUrl ?>?r=/onlineprofile/index" >
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                        <li style="text-align: center;">
                            <a href="<?= \yii\helpers\Url::to(['site/logout'])?>" data-method="post">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                <p>Log out</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>
          <?php }?>
            <div class="content">
            <div class="container-fluid">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
            </div>
  <?php  if(!Yii::$app->user->isGuest){ ?>
<footer class="footer">
            <div class="container-fluid">
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">MLONE</a>, made with love for a better web
                </p>
            </div>
        </footer>
         <?php }?>
</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
