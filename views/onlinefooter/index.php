<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Online-Footer';

?>
<div class="row">
	<div class="online-page-title">
		<h2>Footer Panel</h2>
	</div>
	<div class="card">
		<div class="footer">
			<?php $form = ActiveForm::begin(); ?>
<div class="row clearfix">
    <div class="col-md-6">
     <div class="form-group">
    <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'file')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/footer-image/" . $model->footer_image;
    if(file_exists($path)){
    ?><label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/footer-image/".$model->footer_image?>" />
    <?php
}
echo $form->field($model,'file')->fileInput(['maxlength'=> true]);
}
    
    ?>
    </div>
    </div>

    <div class="col-md-6">
    <div class="form-group">
    <?= $form->field($model, 'footer_descrip')->textarea(['rows' => 6]) ?>
    </div>
    </div>
</div>


<div class="row clearfix">
    <div class="col-md-4">
    <div class="form-group">
    <?= $form->field($model, 'social_link1')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
    <?= $form->field($model, 'social_link3')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
    <?= $form->field($model, 'socail_link2')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
      <div class="form-group">
        <?= $form->field($model, 'social_link4')->textInput(['maxlength' => true]) ?>
      </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'social_link5')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'social_link6')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>