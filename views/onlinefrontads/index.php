<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FrontAds */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Online-ads';
?>

<div class="card">
	<div class="front-ads-form">
	<div class="online-page-title">
		<h2>Edit ads</h2>
	</div>

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<?= $form->field($model, 'slider_ads')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			 <?= $form->field($model, 'front_one')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<?= $form->field($model, 'front_two')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<?= $form->field($model, 'front_three')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<?= $form->field($model, 'front_four')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<?= $form->field($model, 'front_five')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			 <?= $form->field($model, 'front_six')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<?= $form->field($model, 'front_eight')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
</div>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
