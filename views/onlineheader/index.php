<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Online-Footer';

?>
<div class="card">
	<div class="online-page-title">
		<h2>Header Panel</h2>
	</div>
	<div class="header-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>
       <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'file')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/header-image/" . $model->header;
    if(file_exists($path)){
    ?>
    <label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/header-image/".$model->header?>" />
    <?php
}
echo $form->field($model,'file')->fileInput(['maxlength'=> true]);
}
    
    ?>

    <?= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'extra')->textInput(['maxlength' => true]) ?>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>