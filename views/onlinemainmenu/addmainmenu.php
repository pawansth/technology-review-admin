<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CourseType */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card">
	<div class="back-btn">
		<a href="<?=Yii::$app->homeUrl ?>?r=/onlinemainmenu/index"><button class="btn">Back</button></a>
	</div>
	<div class="add-title">
		<h2>Add main menu</h2>
	</div>
	<div class="categories-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'priority')->textInput() ?>
     <?= $form->field($model, 'status')->textInput() ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn ']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
