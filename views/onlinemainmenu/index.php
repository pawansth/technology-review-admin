
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'main menu';

?>
<!-- code of the main categories edit part-->
<div class="list-grid">
    
    <div class=" col-md-12" >
        <div class="back-btn">
        <a href="<?=Yii::$app->homeUrl ?>?r=/site/forproduct"><button class="btn">Back</button></a>
    </div>
         <p class="edit-data" >
    <?= Html::a('Create coursetype', ['addmainmenu'], ['class' => 'btn ']) ?>
  </p>
        <div class="list-table" >
            <div class="sub-list-title">
            <h3>List of Main Categories </h3>
            </div> 
          
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'priority',
            // 'status',


            ['class' => 'yii\grid\ActionColumn','contentOptions'=>['style'=>'width:165px'] ,'template' => '{view} {update} {delete}',

            'buttons' => [
                'view' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>View</span>", ['onlinemainmenu/viewmainmenu','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'update' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Update</span>", ['onlinemainmenu/mainmenuupdate','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'delete' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Delete</span>", ['onlinemainmenu/mainmenudelete','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
         
            ]
        ],


        ],
    ]); ?>
        </div>
    </div>

</div>
<!-- end part of the main categories part -->

