
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';

?>


<!-- code part of the sub categories part -->
    <div class="list-grid">
    
 

    <div class=" col-md-12" >
        <div class="back-btn">
        <a href="<?=Yii::$app->homeUrl ?>?r=/site/forproduct"><button class="btn">Back</button></a>
    </div>
         <p class="edit-data" >
    <?= Html::a('Create Product', ['addproduct'], ['class' => 'btn ']) ?>
  </p>
        <div class="list-table" >
            <div class="sub-list-title">
            <h3>List of Product </h3>
            </div> 
          
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'categories_menu',
                'value'=>'categoriesMenu.name',
            ],
            [
                'attribute'=>'sub_menu',
                'value'=>'subMenu.name',
            ],
             // 'id',
            'product_name',
            'product_price',
            'product_brand',
            // 'product_subdescrip:ntext',
            //'affiliate_link1',
            //'affiliate_link2',
            //'affiliate_link3',
            //'affiliate_link4',
            //'long_descrip:ntext',
            //'detail:ntext',
           
            //'status',
            //'product_image1',
            //'product_image2',
            //'product_image3',
            //'product_image4',
           
            //'affiliate_price',
            //'affiliate_price1',
           


            ['class' => 'yii\grid\ActionColumn','contentOptions'=>['style'=>'width:165px'] ,'template' => '{view} {update} {delete}',

            'buttons' => [
                'view' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>View</span>", ['onlineproduct/viewproduct','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'update' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Update</span>", ['onlineproduct/productupdate','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'delete' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Delete</span>", ['onlineproduct/productdelete','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
         
            ]
        ],


        ],
    ]); ?>
        </div>
    </div>

</div>
<!-- end code part of the sub categories part -->