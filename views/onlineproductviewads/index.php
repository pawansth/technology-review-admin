<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductviewAds */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card">
<div class="productview-ads-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'view_one')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
     <div class="col-md-4">
        <div class="form-group">
             <?= $form->field($model, 'view_two')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
     <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'view_three')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'view_four')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
     <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'view_five')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
     <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'view_six')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
