<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Online-Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
                        <div class="col-md-8">
                        	  <?php $form = ActiveForm::begin(); ?>
                            <div class="card">
                                <div class="profile-detail">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Profile</h4>
                                </div>
                                <div class="card-body">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-5 pr-1">
                                                <div class="form-group">
                                                    <label>School Name</label>
                                                    <input type="text" class="form-control" disabled="" placeholder="Company" value="Creative Code Inc.">
                                                </div>
                                            </div>
                                            <div class="col-md-3 px-1">
                                                <div class="form-group">
                                                   
                                                    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4 pl-1">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 pr-1">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 pr-1">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'dob')->textInput(['class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                            

                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 pr-1">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'per_address')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 pr-1">
                                                <div class="form-group">
                                                   <?= $form->field($model, 'temp_address')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 pr-1">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4 pr-1">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'phone_number2')->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <?= $form->field($model, 'detail')->textarea(['rows' => 6,'class'=>'form-control']) ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                     <div class="form-group">
                                                     <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'file')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/profile/" . $model->image;
    if(file_exists($path)){
    ?><label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/profile/".$model->image?>" />
    <?php
}
echo $form->field($model,'file')->fileInput(['maxlength'=> true]);
}
    
    ?>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Update Profile' : 'Update', ['class' => $model->isNewRecord ? 'btn   pull-right' : 'btn']) ?>
        
    </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                            </div>
                             

    <?php ActiveForm::end(); ?>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-user">
                                <?php {?>
                                <div class="card-image">
                                    <img src="/basic/web/image/profile/<?= $model ->image ?>" class="img-responsive" alt="...">
                                </div>
                                <div class="card-body">
                                    <div class="author">
                                        <a href="#">
                                            <img class="avatar border-gray" src="/basic/web/image/profile/<?= $model->image ?>" alt="...">
                                            <h5 class="title"><?= $model ->full_name ?></h5>
                                        </a>
                                        <p class="description">
                                            <?= $model->username ?>
                                        </p>
                                    </div>
                                    <p class="description text-center">
                                        "Lamborghini Mercy
                                        <br> Your chick she so thirsty
                                        <br> I'm in that two seat Lambo"
                                    </p>
                                </div>
                                <hr>
                                <div class="button-container mr-auto ml-auto">
                                    <button href="#" class="btn btn-simple btn-link btn-icon">
                                        <i class="fa fa-facebook-square"></i>
                                    </button>
                                    <button href="#" class="btn btn-simple btn-link btn-icon">
                                        <i class="fa fa-twitter"></i>
                                    </button>
                                    <button href="#" class="btn btn-simple btn-link btn-icon">
                                        <i class="fa fa-google-plus-square"></i>
                                    </button>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>