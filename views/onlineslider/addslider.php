<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card">
    <div class="back-btn">
        <a href="<?=Yii::$app->homeUrl ?>?r=/onlineslider/index"><button class="btn">Back</button></a>
    </div>
    <div class="add-title">
        <h2>Add Slider</h2>
    </div>
<div class="slider-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
             <?= $form->field($model, 'product_price')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
             <?= $form->field($model, 'product_brand')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <?= $form->field($model, 'affiliate_link1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?= $form->field($model, 'affiliate_link2')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
             <?= $form->field($model, 'affiliate_link3')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?= $form->field($model, 'affiliate_link4')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?= $form->field($model, 'affiliate_price1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <?= $form->field($model, 'affiliate_price2')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>
<div class="row">
    
    <div class="col-md-3">
        <div class="form-group">
     <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'files')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/slider/" . $model->product_image1;
    if(file_exists($path)){
    ?><label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/slider/".$model->product_image1 ?>" />
    <?php
}
echo $form->field($model,'files')->fileInput(['maxlength'=> true]);
}
    
    ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'file1')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/slider/" . $model->product_image2;
    if(file_exists($path)){
    ?><label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/slider/".$model->product_image2 ?>" />
    <?php
}
echo $form->field($model,'file1')->fileInput(['maxlength'=> true]);
}
    
    ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
             <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'file2')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/slider/" . $model->product_image3;
    if(file_exists($path)){
    ?><label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/slider/".$model->product_image3 ?>" />
    <?php
}
echo $form->field($model,'file2')->fileInput(['maxlength'=> true]);
}
    
    ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'file3')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/slider/" . $model->product_image4;
    if(file_exists($path)){
    ?><label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/slider/".$model->product_image4 ?>" />
    <?php
}
echo $form->field($model,'file3')->fileInput(['maxlength'=> true]);
}
    
    ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <?php
if($model->isNewRecord)
{

    echo $form->field($model, 'file')->fileInput(['maxlength' => true]);
}
else
{
    $path= Yii::$app->basepath . "/web/image/slider/" . $model->slider_image;
    if(file_exists($path)){
    ?><label>image</label>
    <img class="img-responsive" style="height:100px" src="<?=Yii::getAlias('@web')."/image/slider/".$model->slider_image ?>" />
    <?php
}
echo $form->field($model,'file')->fileInput(['maxlength'=> true]);
}
    
    ?>
           
        </div>
    </div>
    
    
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
           
            <label>Sub Description</label>
    <?php echo froala\froalaeditor\FroalaEditorWidget::widget([
    'model' => $model,
    'attribute' => 'product_subdescrip',
    'options' => [
        // html attributes
        'id'=>'slider2'
    ],
    'clientOptions' => [
        'toolbarInline' => false,
        'theme' => 'royal', //optional: dark, red, gray, royal
        'language' => 'en_gb' // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...
    ]
]); ?>
        </div>
        <div class="form-group">
           
            <label>Key Specification</label>
    <?php echo froala\froalaeditor\FroalaEditorWidget::widget([
    'model' => $model,
    'attribute' => 'detail',
    'options' => [
        // html attributes
        'id'=>'slider4'
    ],
    'clientOptions' => [
        'toolbarInline' => false,
        'theme' => 'royal', //optional: dark, red, gray, royal
        'language' => 'en_gb' // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...
    ]
]); ?>
        </div>
    </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
           
            <label>Description for Products</label>
    <?php echo froala\froalaeditor\FroalaEditorWidget::widget([
    'model' => $model,
    'attribute' => 'long_descrip',
    'options' => [
        // html attributes
        'id'=>'slider3'
    ],
    'clientOptions' => [
        'toolbarInline' => false,
        'theme' => 'royal', //optional: dark, red, gray, royal
        'language' => 'en_gb' // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...
    ]
]); ?>
        </div>
    </div>
</div>



    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
