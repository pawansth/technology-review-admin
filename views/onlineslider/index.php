
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';

?>


<!-- code part of the slider part -->
    <div class="list-grid">
    
 	<div class="add-title">
        <h2>Slider </h2>
    </div>
    <hr>
    <div class=" col-md-12" >
       
         <p class="edit-data" >
    <?= Html::a('Create Slider', ['addslider'], ['class' => 'btn ']) ?>
  </p>
        <div class="list-table" >
            <div class="sub-list-title">
            <h3>List of Sliders </h3>
            </div> 
          
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'title',
            'product_name',
            'product_price',
            'product_brand',
            //'product_subdescrip:ntext',
            //'affiliate_link1',
            //'affiliate_link2',
            //'affiliate_link3',
            //'affiliate_link4',
            //'long_descrip:ntext',
            //'detail:ntext',
            //'slider_image',
            //'product_image1',
            //'product_image2',
            //'product_image3',
            //'product_image4',
            //'status',


            ['class' => 'yii\grid\ActionColumn','contentOptions'=>['style'=>'width:165px'] ,'template' => '{view} {update} {delete}',

            'buttons' => [
                'view' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>View</span>", ['onlineslider/viewslider','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'update' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Update</span>", ['onlineslider/sliderupdate','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'delete' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Delete</span>", ['onlineslider/sliderdelete','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
         
            ]
        ],


        ],
    ]); ?>
        </div>
    </div>

</div>
<!-- end code part of the slider part -->