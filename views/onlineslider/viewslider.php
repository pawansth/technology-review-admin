<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['sliderupdate', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['sliderdelete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'product_name',
            'product_price',
            'product_brand',
            'product_subdescrip:ntext',
            'affiliate_link1',
            'affiliate_link2',
            'affiliate_link3',
            'affiliate_link4',
            'long_descrip:ntext',
            'detail:ntext',
            'slider_image',
            'product_image1',
            'product_image2',
            'product_image3',
            'product_image4',
            'status',
        ],
    ]) ?>

</div>
