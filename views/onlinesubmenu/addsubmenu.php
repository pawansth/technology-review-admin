<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\CategoriesSubmenu */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card">
	<div class="back-btn">
		<a href="<?=Yii::$app->homeUrl ?>?r=/onlinesubmenu/index"><button class="btn">Back</button></a>
	</div>
	<div class="add-title">
		<h2>Add Sub menu</h2>
	</div>

<div class="categories-submenu-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="form-group">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
</div>
<div class="form-group">
    <?= $form->field($model, 'priority')->textInput() ?>
</div>
<div class="form-group">
   <?php 
     $menutype = \yii\helpers\ArrayHelper::map(app\models\CategoriesMenu::find()->where(['status'=>1])->all(), 'id','name');
     echo $form->field($model, 'categories_menu')->widget(select2::classname(), [
         'data'=>$menutype,
        'options'=> [
            'placeholder'=>'select main menu',
            'id'=>'CategoriesMenu'],
            'pluginOptions' =>[
            'allowclear'=>true
        ],
     ]);
     ?>
</div>
    
 <?= $form->field($model, 'status')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
