
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub menu';

?>


<!-- code part of the sub categories part -->
    <div class="list-grid">
    
 

    <div class=" col-md-12" >
        <div class="back-btn">
        <a href="<?=Yii::$app->homeUrl ?>?r=/site/forproduct"><button class="btn">Back</button></a>
    </div>
         <p class="edit-data" >
    <?= Html::a('Create Categories Submenu', ['addsubmenu'], ['class' => 'btn ']) ?>
  </p>
        <div class="list-table" >
            <div class="sub-list-title">
            <h3>List of coursetype </h3>
            </div> 
          
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'priority',
            'categoriesMenu.name',

            // 'status',


            ['class' => 'yii\grid\ActionColumn','contentOptions'=>['style'=>'width:165px'] ,'template' => '{view} {update} {delete}',

            'buttons' => [
                'view' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>View</span>", ['onlinesubmenu/viewsubmenu','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'update' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Update</span>", ['onlinesubmenu/submenuupdate','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
                            'delete' => function($url, $model, $key) {

                    return Html::a("<span class='btn  btn-xs'>Delete</span>", ['onlinesubmenu/submenudelete','id' => $key], [
                        'title' => \yii::t('yii', 'print')
                    ]);

                }, 
         
            ]
        ],


        ],
    ]); ?>
        </div>
    </div>

</div>
<!-- end code part of the sub categories part -->