<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'product_name') ?>

    <?= $form->field($model, 'product_price') ?>

    <?= $form->field($model, 'product_brand') ?>

    <?= $form->field($model, 'product_subdescrip') ?>

    <?php // echo $form->field($model, 'affiliate_link1') ?>

    <?php // echo $form->field($model, 'affiliate_link2') ?>

    <?php // echo $form->field($model, 'affiliate_link3') ?>

    <?php // echo $form->field($model, 'affiliate_link4') ?>

    <?php // echo $form->field($model, 'long_descrip') ?>

    <?php // echo $form->field($model, 'detail') ?>

    <?php // echo $form->field($model, 'categories_menu') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'product_image1') ?>

    <?php // echo $form->field($model, 'product_image2') ?>

    <?php // echo $form->field($model, 'product_image3') ?>

    <?php // echo $form->field($model, 'product_image4') ?>

    <?php // echo $form->field($model, 'sub_menu') ?>

    <?php // echo $form->field($model, 'affiliate_price') ?>

    <?php // echo $form->field($model, 'affiliate_price1') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
