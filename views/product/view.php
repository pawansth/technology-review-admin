<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_name',
            'product_price',
            'product_brand',
            'product_subdescrip:ntext',
            'affiliate_link1',
            'affiliate_link2',
            'affiliate_link3',
            'affiliate_link4',
            'long_descrip:ntext',
            'detail:ntext',
            'categories_menu',
            'status',
            'product_image1',
            'product_image2',
            'product_image3',
            'product_image4',
            'sub_menu',
            'affiliate_price',
            'affiliate_price1',
        ],
    ]) ?>

</div>
