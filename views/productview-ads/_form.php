<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductviewAds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productview-ads-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'view_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'view_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'view_three')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'view_four')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'view_five')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'view_six')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
