<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductviewAdsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productview-ads-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'view_one') ?>

    <?= $form->field($model, 'view_two') ?>

    <?= $form->field($model, 'view_three') ?>

    <?= $form->field($model, 'view_four') ?>

    <?php // echo $form->field($model, 'view_five') ?>

    <?php // echo $form->field($model, 'view_six') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
