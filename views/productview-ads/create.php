<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductviewAds */

$this->title = 'Create Productview Ads';
$this->params['breadcrumbs'][] = ['label' => 'Productview Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productview-ads-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
