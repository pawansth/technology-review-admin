<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductviewAds */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Productview Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productview-ads-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'view_one',
            'view_two',
            'view_three',
            'view_four',
            'view_five',
            'view_six',
            'status',
        ],
    ]) ?>

</div>
