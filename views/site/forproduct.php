<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="product-menu">
				<a href="<?=Yii::$app->homeUrl ?>?r=/onlinemainmenu/index"><h1>Main Menu</h1></a>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="card">
			<div class="product-menu">
				<a href="<?=Yii::$app->homeUrl ?>?r=/onlinesubmenu/index"><h1>Sub Menu</h1></a>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="card">
			<div class="product-menu">
				<a href="<?=Yii::$app->homeUrl ?>?r=/onlineproduct/index"><h1>Products</h1></a>
			</div>
		</div>
	</div>



</div>

