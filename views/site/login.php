<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="site-login">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

     
<div class="wrapperss">
  <div class="login-head">
    <h2>Online</h2>
  </div>
  <div class="login">
    <p class="title">Log in</p>
    <input type="text" placeholder="Username" name="LoginForm[username]" autofocus/>
    <i class="fa fa-user"></i>
    <input type="password" placeholder="Password" name="LoginForm[password]" />
    <i class="fa fa-key"></i>
    <button>
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </button>
  </div>
  <!-- <footer><a target="blank" href="http://boudra.me/">boudra.me</a></footer> -->
</div>

    <?php ActiveForm::end(); ?>
</div>
<!-- <script type="text/javascript">
  var working = false;
$('.login').on('submit', function(e) {
  e.preventDefault();
  if (working) return;
  working = true;
  var $this = $(this),
    $state = $this.find('button > .state');
  $this.addClass('loading');
  $state.html('Authenticating');
  setTimeout(function() {
    $this.addClass('ok');
    $state.html('Welcome back!');
    setTimeout(function() {
      $state.html('Log in');
      $this.removeClass('ok loading');
      working = false;
    }, 4000);
  }, 3000);
});
</script> -->