<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_brand')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_subdescrip')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'affiliate_link1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'affiliate_link2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'affiliate_link3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'affiliate_link4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'long_descrip')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'slider_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_image1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_image2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_image3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_image4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
