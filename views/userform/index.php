<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Userforms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userform-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Userform', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'full_name',
            'per_address',
            'temp_address',
            'dob',
            //'phone_number',
            //'username',
            //'password',
            //'email:email',
            //'image',
            //'phone_number2',
            //'detail:ntext',
            //'authKey',
            //'token',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
