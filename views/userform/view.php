<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Userform */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Userforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userform-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'per_address',
            'temp_address',
            'dob',
            'phone_number',
            'username',
            'password',
            'email:email',
            'image',
            'phone_number2',
            'detail:ntext',
            'authKey',
            'token',
            'status',
        ],
    ]) ?>

</div>
